# RSP_echo_bot

---

```
This is simple echo bot for Telegram:

-->  It replies with same message as the user send
-->  It uses PostgreSQL DB to save some logs about a users what write to it. 
```

---

### ROADMAP

```
Some new features will be added later.
```

---

### Contact information:

##### Author name: ``` Serhii Ryzhaiev```

##### Email: [rsp89.we@gmail.com](mailto:rsp89.we@gmail.com?subject=RSP_echo_bot)

##### Telegram: [Serhii Ryzhaiev](https://t.me/CTAJIKEP)

---

### Contributing:

```
If you're have any questions or any suggestions, feel free to contact me by given contact info.
```
