import os
from dataclasses import dataclass

from dotenv import load_dotenv

load_dotenv()


@dataclass
class Settings:
    BOT_NAME: str = os.environ.get("BOT_NAME")
    BOT_TOKEN: str = os.environ.get("BOT_TOKEN")
    BOT_USERNAME: str = os.environ.get("BOT_USERNAME")

    POSTGRES_NAME: str = os.environ.get("POSTGRES_DB")
    POSTGRES_USER: str = os.environ.get("POSTGRES_USER")
    POSTGRES_PASSWORD: str = os.environ.get("POSTGRES_PASSWORD")
    POSTGRES_HOST: str = os.environ.get("POSTGRES_HOST")
    POSTGRES_PORT_HOST: str = os.environ.get("POSTGRES_PORT_HOST")


settings = Settings()
